extern crate termion;

use std::io::{stdin, stdout, Write};
use termion::event::{Event, Key, MouseEvent};
use termion::input::{MouseTerminal, TermRead};
use termion::raw::IntoRawMode;

trait GameConfigTrait {
    type Sprite;
}

struct GameConfigConsole();

impl GameConfigTrait for GameConfigConsole {
    type Sprite = String;
}

trait GraphicsContext {
    type Sprite;

    type Error;

    fn clear(&mut self) -> Result<(), Self::Error>;

    fn show_cursor(&mut self) -> Result<(), Self::Error>;

    fn hide_cursor(&mut self) -> Result<(), Self::Error>;

    fn put(&mut self, sprite: &Self::Sprite, x: i64, y: i64) -> Result<(), Self::Error>;
}

struct ConsoleGraphicsContext<W: std::io::Write> {
    stdout: MouseTerminal<termion::raw::RawTerminal<W>>,
}

impl<W: std::io::Write> ConsoleGraphicsContext<W> {
    fn new(term: termion::raw::RawTerminal<W>) -> Self {
        ConsoleGraphicsContext {
            stdout: MouseTerminal::from(term),
        }
    }
}

impl<W: std::io::Write> GraphicsContext for ConsoleGraphicsContext<W> {
    type Sprite = String;

    type Error = std::io::Error;

    fn clear(&mut self) -> Result<(), Self::Error> {
        write!(self.stdout, "{}", termion::clear::All)?;
        self.stdout.flush()
    }

    fn show_cursor(&mut self) -> Result<(), Self::Error> {
        write!(self.stdout, "{}", termion::cursor::Show)?;
        self.stdout.flush()
    }

    fn hide_cursor(&mut self) -> Result<(), Self::Error> {
        write!(self.stdout, "{}", termion::cursor::Hide)?;
        self.stdout.flush()
    }

    fn put(&mut self, sprite: &Self::Sprite, x: i64, y: i64) -> Result<(), std::io::Error> {
        write!(
            self.stdout,
            "{}{}",
            termion::cursor::Goto(x as u16, y as u16),
            sprite
        );
        self.stdout.flush()
    }
}

trait Entity<S> {
    fn update(
        &mut self,
        a: &Action,
        graphics_context: &mut GraphicsContext<Sprite = S, Error = std::io::Error>,
    );
}

struct Game<Config: GameConfigTrait> {
    entities: Vec<Box<Entity<Config::Sprite>>>,
}

impl<Config: GameConfigTrait> Game<Config> {
    fn new() -> Self {
        Game {
            entities: Vec::new(),
        }
    }

    fn add_entity(&mut self, e: Box<Entity<Config::Sprite>>) {
        self.entities.push(e)
    }

    fn step(
        &mut self,
        action: Action,
        graphics_context: &mut GraphicsContext<Sprite = Config::Sprite, Error = std::io::Error>,
    ) {
        for e in &mut self.entities {
            graphics_context.clear().unwrap();
            e.update(&action, graphics_context);
        }
    }
}

struct InputComponent();

impl InputComponent {
    fn new() -> Self {
        InputComponent()
    }

    fn update(&self, state: &mut EntityState, action: &Action) {
        let (x, y) = state.position();
        match action {
            Action::MoveAbs(x, y) => state.move_to(*x, *y),
            Action::MoveRel(off_x, off_y) => state.move_to(x + *off_x, y + *off_y),
            _ => {}
        }
    }
}

struct GraphicsComponent<S> {
    sprite: S,
}

impl<S> GraphicsComponent<S> {
    fn new(sprite: S) -> Self {
        GraphicsComponent { sprite: sprite }
    }

    fn update(
        &self,
        state: &EntityState,
        graphics_context: &mut GraphicsContext<Sprite = S, Error = std::io::Error>,
    ) {
        graphics_context
            .put(&self.sprite, state.x, state.y)
            .unwrap()
    }
}

struct EntityState {
    x: i64,
    y: i64,
}

struct Player<S> {
    state: EntityState,
    input_component: InputComponent,
    graphics_component: GraphicsComponent<S>,
}

impl<S> Player<S> {
    fn new(input_component: InputComponent, graphics_component: GraphicsComponent<S>) -> Self {
        Player {
            state: EntityState { x: 1, y: 1 },
            input_component: input_component,
            graphics_component: graphics_component,
        }
    }
}

impl EntityState {
    fn position(&self) -> (i64, i64) {
        (self.x, self.y)
    }

    fn move_to(&mut self, x: i64, y: i64) {
        self.x = x;
        self.y = y;
    }
}

impl<S> Entity<S> for Player<S> {
    fn update(
        &mut self,
        action: &Action,
        graphics_context: &mut GraphicsContext<Sprite = S, Error = std::io::Error>,
    ) {
        self.input_component.update(&mut self.state, action);
        self.graphics_component
            .update(&self.state, graphics_context);
    }
}

#[derive(Eq, PartialEq)]
enum Action {
    MoveAbs(i64, i64),
    MoveRel(i64, i64),
    Unknown,
    Quit,
}

fn get_action(evt: Event) -> Action {
    match evt {
        Event::Key(Key::Char('q')) => Action::Quit,
        Event::Key(Key::Up) => Action::MoveRel(0, -1),
        Event::Key(Key::Down) => Action::MoveRel(0, 1),
        Event::Key(Key::Left) => Action::MoveRel(-1, 0),
        Event::Key(Key::Right) => Action::MoveRel(1, 0),
        Event::Mouse(MouseEvent::Press(_, x, y)) => Action::MoveAbs(x as i64, y as i64),
        _ => Action::Unknown,
    }
}

fn main() {
    let stdin = stdin();
    let raw_stdout = stdout().into_raw_mode().unwrap();
    let mut gc = ConsoleGraphicsContext::new(raw_stdout);
    let mut game = Game::<GameConfigConsole>::new();
    let mut player = Player::new(
        InputComponent::new(),
        GraphicsComponent::new("@".to_string()),
    );

    gc.clear().unwrap();
    gc.hide_cursor().unwrap();

    player.update(&Action::Unknown, &mut gc);

    game.add_entity(Box::new(player));

    let actions = stdin.events().map(|e| get_action(e.unwrap()));
    for a in actions.take_while(|a| *a != Action::Quit) {
        game.step(a, &mut gc)
    }

    gc.show_cursor().unwrap();
}
